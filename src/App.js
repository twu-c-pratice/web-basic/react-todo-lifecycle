import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {isShowList: false};
    this.switchStatus = this.switchStatus.bind(this);
    this.refresh = this.refresh.bind(this);
  }


  render() {
    const isShowList = this.state.isShowList;
    let button = <button onClick={this.switchStatus}>Show</button>;
    if (isShowList) {
      button = <button onClick={this.switchStatus}>Hide</button>;
    }

    return (
        <div>
          {button}
          <button onClick={this.refresh}>Refresh</button>
          {isShowList ? (<TodoList/>) : null}
        </div>
    );
  }

  switchStatus() {
    let isShowList = this.state.isShowList;
    this.setState({isShowList: !(isShowList)});
  }

  refresh() {
    this.forceUpdate();
  }
}

export default App;