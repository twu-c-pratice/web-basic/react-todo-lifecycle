import React, {Component} from 'react';
import './todolist.less';

class TodoList extends Component {
  constructor(props) {
    super(props);
    console.log('constructor was called!');
    this.state = {listLength: 0};
    this.listItems = this.listItems.bind(this);
    this.addItemLength = this.addItemLength.bind(this);
  }

  render() {
    console.log("render was called!");
    return (
        <div>
          <button onClick={this.addItemLength}>Add</button>
          <ul>
            {this.listItems()}
          </ul>
        </div>
    );
  }

  addItemLength() {
    let length = this.state.listLength;
    this.setState({listLength: length + 1});
  }

  listItems() {
    let list = [];
    for (let i = 1; i <= this.state.listLength; i++) {
      list.push(i);
    }

    return list.map((number) =>
        <li key={number.toString()}>List Item {number}</li>
    );
  }


  componentDidMount() {
    console.log("componentDidMount was called!");
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("componentDidUpdate was called!");
  }

  componentWillUnmount() {
    console.log("componentWillUnmount was called!")
  }
}

export default TodoList;

